const menu = document.querySelector(".hamburger")
const main = document.querySelector(".main")
const menuItems = document.querySelector(".navbar ul")
const mobileMenu = document.querySelector(".mobileMenu")


menu.addEventListener('click', (event) => {
    console.log(event.target)
    if (event.target.tagName === 'A') {
        event.preventDefault()
    } else {

        mobileMenu.innerHTML = menuItems.innerHTML
        menu.classList.toggle('on')
        main.classList.toggle('on')
        console.log('main toggled')
        console.log(menu.classList)

    }
    return
})